//Exercice de création de paniers, et de produits
//Crée unr fonction qui rempli mes inputs d'un produit

//—————————————————-var d'initialisation————————————————————
//Testerrrr
var productTest = {
    ref: 0,
    nom: 'base',
    prix: '0',
    stock: '0'
 }

//Création du panier
var panier = {
    "ref":"P1",
    "items":[],//récupération des produits : produit et quantité que le client veut
    "client":"client1"
}

//Création client
var client = {
    "id": "client1",
    "prenom": "pierre"
}

//————————————————————————Appel des fonctions—————————————————

fillProdTable(panier);
manageNewProd();
manageEvent();





// //Création produit
// var produit = localStorage.getItem("XB411");

var produit = productTest;




//Fonction pour créer le tableau Panier
var panierBody = document.getElementById("bodyPanier");//récupérer la table par son id

//Créer variable qui héberge la longueur dont on veut tenir compte dans la boucle : nb de lignes = nb de produits existants
var nbProd = panier.items.length;
//Créer une boucle pour remplir la table avec les données
let i = 0;
while(i < nbProd) {
    var row = panierBody.insertRow(-1);//Création des lignes, à chaque passage il crée une ligne où on va stocker nos données
    console.log(panier.items[0]);
    var prodPan = JSON.parse(panier.items[i][0]) //dans le premier tour on va récupérer "produit" du premier tableau dans "items":[[produit,2],[produit,3]]

    var cell = row.insertCell(-1)//Pour insérer l'info dans la dernière cellule
    var newText = document.createTextNode(prodPan.nom);//On va remplir notre cellule nom
    cell.appendChild(newText);

    var cell = row.insertCell(-1)//Pour insérer l'info dans la dernière cellule
    var newText = document.createTextNode(panier.items[i][1]);//On va remplir notre cellule quantité, info qu'on trouve dans la variable panier
    cell.appendChild(newText);

    var cell = row.insertCell(-1)//Pour insérer l'info dans la dernière cellule
    var newText = document.createTextNode(prodPan.prix);//On va remplir notre cellule prix
    cell.appendChild(newText);

    var cell = row.insertCell(-1)//Pour insérer l'info dans la dernière cellule
    var newText = document.createTextNode((panier.items[i][1])*(prodPan.prix));//On va remplir le prix total = prix * quantité
    cell.appendChild(newText);

    var cell = row.insertCell(-1)//Pour insérer l'info dans la dernière cellule
    var newButt = document.createElement("button")//On va créer notre boutton et on va remplir avec le boutton Supprimer
    newButt.classList.add("btnSupp"); //Création Classe Boutton
    newButt.id = prodPan.ref;//Création ID boutton ='"+prodPan.ref+"' ça sert pour créer le lien avec l'évenement click Boutton au travers de l'id du produit (ref)
    newButt.innerText = "X";//Création contenu du boutton
    
    cell.appendChild(newButt);  

    //incrémentation 
    i++
}