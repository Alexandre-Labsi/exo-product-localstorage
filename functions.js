
function editProd(key){
    //recup le produit dans le localStorage
    var product = JSON.parse(localStorage.getItem(key));
    // 1) Récupération de l'input nom
    var inputNom = document.getElementById('nom');
    // 2) Ajout de la value
    inputNom.value = product.nom;

    // 1) Récupération de l'input prix
    var inputPrix = document.getElementById('prix');
    // 2) Ajout de la value
    inputPrix.value = product.prix;

    // 1) Récupération de l'input stock
    var inputStock = document.getElementById('stock');
    // 2) Ajout de la value
    inputStock.value = product.stock;

    // 3) Récupérer l'id du bouton +  EventListener
    var clickButton = document.getElementById('edit');
    clickButton.addEventListener('click', ()=> {

    // 4) Réaffecte dans l'objet
    product.nom = inputNom.value;
    product.prix = inputPrix.value;
    product.stock = inputStock.value;

    // 5) Pousser l'objet dans le localStorage
    localStorage.setItem(key, JSON.stringify(product));
    })
}

function suppProd(key){
    localStorage.removeItem(key);
    alert('Produit '+ key +' bien supprimé')
    document.location.reload();
}

function addProd(key, product){
    localStorage.setItem(key, JSON.stringify(product));
    window.location.reload();
}

function fillProdTable(panier){
    //recup la div
    var divTable = document.getElementById('productsList');
    //crée ma table
    var tableList = document.createElement('table');
    tableList.style.border = '1px solid black';
    //add id
    tableList.id = 'productsTable';
    //accrocher la table a la div
    divTable.appendChild(tableList);

    //crée un tr
    var tableRow = document.createElement('tr');

    //Nom des colonnes
    var col = ['reference', 'nom', 'prix', 'stock', '', ''];

    //mes produits
    var prod = [];

    //parcourir le total du localStorage et récupérer ses clés pour afficher tout mes produits
    var keysList = Object.keys(localStorage);

    //boucle dans le tableau de clé
    for(var i=0; i<keysList.length; i++){
        //la clé du localStorage est l'element pendant l'itération
        prod.push(JSON.parse(localStorage.getItem(keysList[i]))); //impossible de concaténer, donc push
    }

    var keysProd = Object.keys(productTest); //introspecter un objet pour trouver ses propriétés -> tableau de propriétés
        
    //Ajouter une ligne en th
    var row = tableList.insertRow(-1);

    for (var i = 0; i < col.length; i++) {
        var headerCell = document.createElement("th");
        headerCell.innerHTML = col[i]; 
        row.appendChild(headerCell);
    }

    //Ajout des data 
    for (var i = 0; i < keysList.length; i++) { 
        //crée une ligne pour chaque produit
        row = tableList.insertRow(-1);

        //replir chaque cellule
        for (var j = 0; j < keysProd.length; j++) {
            //si ma liste de clé boucle et tombe sur la clé 'nom'
            if(keysProd[j] == 'nom'){
                //ajout d'une cell + lien
                var cell = row.insertCell(-1);
                cell.innerHTML = '<a name="'+ prod[i]['ref']+'" href="'+ prod[i]['ref'] +'">' + prod[i][keysProd[j]] + '</a>';
            }else{
                var cell = row.insertCell(-1);
                cell.innerHTML = prod[i][keysProd[j]];
            }
        }
        //Rajout de cellule vide a cause de pierre pour boutons UI
        row.insertCell(-1).innerHTML = "<button class='mod' name='"+ prod[i].ref +"'>Modifier</button>"
        row.insertCell(-1).innerHTML = "<button class='sup' name='"+ prod[i].ref +"'>Supprimer</button>"
    }

    //boucle pour chaque liens
    var allLinks = document.querySelectorAll("a");
    //incrémenteur
    let k = 0;
    while(k < allLinks.length){
        allLinks[k].addEventListener('click', (e)=> {
            e.preventDefault();
            //appel de la function
            sendToPanier(e.toElement.name, panier); 
             //toElement est une methode de l'event, qui recup la nom grace a .name
        })
        //itérateur
        k++
    }

}

function manageNewProd(){
    // //recuperer id du boutton add + eventlistener
var btnAdd = document.getElementById('add');
// eventlistener sur tout mes boutons
    btnAdd.addEventListener('click', ()=> {
        //crée une clé + clé produit
        var product = {};
        var key = 'XB' + Math.floor(Math.random() * 10000);
        product.ref = key;

    // // 1) Récupération de l'input ref
    // var inputRef = document.getElementById('ref');
    // // 2) Ajout de la value
    // product.ref = inputRef.value;

    // 1) Récupération de l'input nom
    var inputNom = document.getElementById('nom');
    // 2) Ajout de la value
    product.nom =  inputNom.value;

    // 1) Récupération de l'input prix
    var inputPrix = document.getElementById('prix');
    // 2) Ajout de la value
    product.prix = inputPrix.value;

    // 1) Récupération de l'input stock
    var inputStock = document.getElementById('stock');
    // 2) Ajout de la value
    product.stock = inputStock.value;

    addProd(key, product);
})
}

function manageEvent(){
    //recuperer id du boutton modifier + eventlistener
    var btnAll = document.getElementsByClassName('mod');
  
    // eventlistener sur tout mes boutons
    for(let i = 0; i < btnAll.length; i++) {
      btnAll[i].addEventListener('click', ()=> {
        editProd(btnAll[i].name);
        
      })
    }
    // //recuperer id du boutton supprimer + eventlistener
    var btnSupp = document.getElementsByClassName('sup');
    // eventlistener sur tout mes boutons
    for(let i = 0; i < btnSupp.length; i++) {
      btnSupp[i].addEventListener('click', ()=> {
        suppProd(btnSupp[i].name);
        
      })
    }
}
  
function sendToPanier(ref, panier){
    var tempProd = JSON.parse(localStorage.getItem(ref));
    //si on a pas chercher on a pas trouvé
    var find = 0;
    //pour trouvé si mon produit est déja dans mon panier j'utilise findObjectInArray();
    for(let i=0; i<panier.items.length; i++){
        var prodIndex = findObjectInArray(panier.items[i][0].ref, ref);
        console.log(prodIndex);
        if(prodIndex == 1 ){
            panier.items[i][1]++;
            find = 1;
        } 
    }
    if(find !== 1){
        var tempTab = [tempProd, 1];
        panier.items.push(tempTab);
    }
    console.log(panier.items);
}


function findObjectInArray(prod, val) {
    if(prod == val){
        return 1;
    }else{
        return -1;
    }
}

